package com.mental_elemental.android.support;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class JsonSerializer<T> implements Serializer<T>
{
    @Override
    public final T deserialize(String string)
    {
        if (string == null)
            return null;

        try
        {
            return deserializeJson(new JSONObject(string));
        }
        catch (JSONException e) {
            System.out.println(e.toString());
            return null;
        }
    }

    @Override
    public final String serialize(T object)
    {
        if (object == null)
            return null;

        try
        {
            return serializeJson(object).toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public abstract T deserializeJson(JSONObject jsonObject, Object... args) throws JSONException;
    public abstract JSONObject serializeJson(T object) throws JSONException;

    public List<T> deserializeJson(JSONArray jsonArray, Object... args) throws JSONException
    {
        List<T> objects = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); ++i)
        {
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            if (jsonObject == null)
                jsonObject = new JSONObject(jsonArray.getString(i));
            objects.add(deserializeJson(jsonObject, args));
        }

        return objects;
    }

    public JSONArray serializeJson(Collection<T> objects) throws JSONException
    {
        JSONArray jsonArray = new JSONArray();
        for (T object : objects)
            jsonArray.put(serializeJson(object));
        return jsonArray;
    }

}
