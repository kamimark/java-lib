package com.mental_elemental.android.support;

import android.os.Looper;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;

public class Binder<T>
{
    public static final int TAG_ID = 0xffffffff;
    private final Collection<View> monitoringViews = new ArrayList<>();
    private final Collection<EventListener<T>> eventListeners = new ArrayList<>();
    private int tagID;

    public Binder(int tagID)
    {
        this.tagID = tagID;
    }

    public interface EventListener<T> {
        boolean onEvent(T object);
    }

    public void trigger(@Nullable T object)
    {
        if (object == null)
            return;

        if (Looper.myLooper() != Looper.getMainLooper())
            throw new RuntimeException("Must be done in Main Looper");

        broadCast(object);
    }

    private void broadCast(T object) {

        Collection<EventListener<T>> toRemove = new ArrayList<>();

        for (EventListener<T> listeners : eventListeners)
            if (listeners.onEvent(object))
                toRemove.add(listeners);

        eventListeners.removeAll(toRemove);

        Collection<View> toRemoveViews = new ArrayList<>();
        for (View view : monitoringViews)
        {
            if (ViewCompat.isAttachedToWindow(view))
            {
                if (((EventListener<T>) view.getTag(tagID)).onEvent(object))
                    toRemoveViews.add(view);
            }
            else
            {
                toRemoveViews.add(view);
            }
        }

        monitoringViews.removeAll(toRemoveViews);
    }

    public void attach(View view, @NonNull EventListener<T> onEventListener)
    {
        Object existingTag = view.getTag(tagID);
        if (existingTag != null && existingTag != onEventListener)
            throw new RuntimeException("Already bound with another binder");

        view.setTag(tagID, onEventListener);
        if (ViewCompat.isAttachedToWindow(view))
            if (!monitoringViews.contains(onEventListener))
                monitoringViews.add(view);

        view.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener()
        {
            @Override
            public void onViewAttachedToWindow(View view)
            {
                System.out.println("AttachStateChangeListener Attached " + view.toString());
                if (!monitoringViews.contains(view))
                    monitoringViews.add(view);
            }

            @Override
            public void onViewDetachedFromWindow(View view)
            {
                System.out.println("AttachStateChangeListener Detached " + view.toString());
            }
        });
    }

    public void attach(@NonNull EventListener<T> onEventListener)
    {
        eventListeners.add(onEventListener);
    }

    public void detach(View view)
    {
        monitoringViews.remove(view);
    }

    public void detach(@NonNull EventListener<T> onEventListener)
    {
        eventListeners.remove(onEventListener);
    }

}
