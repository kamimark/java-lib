package com.mental_elemental.android.support.layoutmanagers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AutoGridLayoutManager extends GridLayoutManager
{
    private boolean isSpanSet = false;
    private final int itemWidth;
    private final int itemHeight;

    public AutoGridLayoutManager(RecyclerView recyclerView, int res)
    {
        this(recyclerView, res, RecyclerView.VERTICAL, false);
    }

    public AutoGridLayoutManager(RecyclerView recyclerView, int res, int orientation, boolean reverseLayout)
    {
        super(recyclerView.getContext(), 100, orientation, reverseLayout);
        Context context = recyclerView.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context, orientation, reverseLayout));
        View view = LayoutInflater.from(context).inflate(res, recyclerView, false);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        itemHeight = view.getMeasuredHeight();
        itemWidth = view.getMeasuredWidth();
        recyclerView.setLayoutManager(this);
    }

    private void setSpan()
    {
        if (!isSpanSet)
        {
            if (getOrientation() == VERTICAL)
                setSpanCount(getHorizontalSpace() / itemWidth);
            else
                setSpanCount(getVerticalSpace() / itemHeight);
            isSpanSet = true;
        }
    }

    private int getHorizontalSpace() {
        return getWidth() - getPaddingRight() - getPaddingLeft();
    }

    private int getVerticalSpace() {
        return getHeight() - getPaddingBottom() - getPaddingTop();
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams()
    {
        setSpan();
        return new LayoutParams(itemWidth, itemHeight);
    }

    @Override
    public RecyclerView.LayoutParams generateLayoutParams(Context c, AttributeSet attrs)
    {
        setSpan();
        return new LayoutParams(itemWidth, itemHeight);
    }

    @Override
    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams lp)
    {
        setSpan();
        return new LayoutParams(itemWidth, itemHeight);
    }
}