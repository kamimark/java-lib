package com.mental_elemental.android.support.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class SimpleBaseAdapter extends BaseAdapter
{
    public final int[] items;

    public SimpleBaseAdapter(int[] items)
    {
        this.items = items;
    }

    @Override
    public int getCount()
    {
        return items.length;
    }

    @Override
    public Object getItem(int i)
    {
        return items[i];
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        if (view == null)
            view = createView(viewGroup);

        bindView(view, items[i]);
        return view;
    }

    protected abstract void bindView(View view, int item);
    protected abstract View createView(ViewGroup viewGroup);
}
