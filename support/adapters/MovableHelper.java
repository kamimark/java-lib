package com.mental_elemental.android.support.adapters;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public abstract class MovableHelper<VH extends RecyclerView.ViewHolder>
{
    public static final int PREVENT_MOVEMENT_TYPE = 1;
    public final RecyclerView recyclerView;
    public final RecyclerView.Adapter<VH> adapter;
    public ItemTouchHelper itemTouchHelper;
    public final int movementFlags;
    public boolean isLongPressDragEnabled = false;
    public boolean isItemViewSwipeEnabled = false;

    public MovableHelper(RecyclerView recyclerView, RecyclerView.Adapter<VH> adapter)
    {
        this(recyclerView, adapter, ItemTouchHelper.UP | ItemTouchHelper.DOWN);
    }

    public MovableHelper(RecyclerView recyclerView, RecyclerView.Adapter<VH> adapter, int movementFlags)
    {
        this.adapter = adapter;
        this.recyclerView = recyclerView;
        this.movementFlags = movementFlags;
        itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    protected ItemTouchHelper.Callback itemTouchHelperCallback = new ItemTouchHelper.Callback()
    {
        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            if (viewHolder.getItemViewType() == PREVENT_MOVEMENT_TYPE)
                return 0;
            if (viewHolder.getItemViewType() != 0)
                return makeMovementFlags(viewHolder.getItemViewType(), 0);
            return makeMovementFlags(movementFlags, 0);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
            if (source.getItemViewType() != target.getItemViewType() || !allowMoving(source))
                return false;

            int fromPosition = source.getAdapterPosition();
            int toPosition = target.getAdapterPosition();
            if (!MovableHelper.this.onMoving(fromPosition, toPosition))
                return false;

            adapter.notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return isLongPressDragEnabled;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return isItemViewSwipeEnabled;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {

        }
    };

    @SuppressLint("ClickableViewAccessibility")
    public void bindHandle(RecyclerView.ViewHolder viewHolder, View handleView)
    {
        handleView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                itemTouchHelper.startDrag(viewHolder);
            return false;
        });
    }

    protected boolean allowMoving(RecyclerView.ViewHolder viewHolder)
    {
        return true;
    }

    protected abstract boolean onMoving(int fromPosition, int toPosition);
}