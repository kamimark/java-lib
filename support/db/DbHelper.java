package com.mental_elemental.android.support.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class DbHelper extends SQLiteOpenHelper
{
    public static class Projection
    {
        String type;
        String key;
        String data;
    }

    public final String[] TABLES;

    public static class CacheEntry implements BaseColumns
    {
        public static final String COLUMN_NAME_KEY = "key";
        public static final String COLUMN_NAME_VALUE = "value";
    }

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "dbHelper.db";

    String[] projection = {
            CacheEntry.COLUMN_NAME_KEY,
            CacheEntry.COLUMN_NAME_VALUE
    };

    String selection = CacheEntry.COLUMN_NAME_KEY + " = ?";
    String query = CacheEntry.COLUMN_NAME_KEY + " like ?";

    public DbHelper(Context context, String[] tables)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        TABLES = tables;
    }

    public void onCreate(SQLiteDatabase db)
    {
        for (String table : TABLES)
            db.execSQL(getCreateTable(table));
    }

    private String getCreateTable(String table)
    {
        return "CREATE TABLE IF NOT EXISTS " + table + " (" +
                        CacheEntry.COLUMN_NAME_KEY + " TEXT PRIMARY KEY," +
                        CacheEntry.COLUMN_NAME_VALUE + " TEXT)";
    }


    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        //for (String table : TABLES)
        //    db.execSQL("DROP TABLE IF EXISTS " + table);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db, oldVersion, newVersion);
    }

    void remove(final String type, final String key)
    {
        if (key == null)
        {
            clear(type);
        }
        else
        {
            SQLiteDatabase db = getWritableDatabase();
            if (db.delete(type, selection, new String[]{key}) == 0 && key.isEmpty())
                db.delete(type, selection, new String[]{null}); // Fallback
        }
    }

    void clear(String type)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(type, null, null);
    }

    void set(final String type, final String key, final String data)
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(CacheEntry.COLUMN_NAME_KEY, key);
        contentValues.put(CacheEntry.COLUMN_NAME_VALUE, data);

        db.insertWithOnConflict(type, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    void setAll(String type, Collection<Projection> projections)
    {
        SQLiteDatabase db = getWritableDatabase();

        for (Projection projection : projections)
        {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CacheEntry.COLUMN_NAME_KEY, projection.key);
            contentValues.put(CacheEntry.COLUMN_NAME_VALUE, projection.data);

            db.insertWithOnConflict(type, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        }
    }

    Projection get(String type, String key)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                type,
                projection,
                selection,
                new String[]{key},
                null,
                null,
                null);

        Projection projection = new Projection();
        projection.type = type;
        projection.key = key;

        if (cursor.moveToNext())
            projection.data = cursor.getString(cursor.getColumnIndexOrThrow(CacheEntry.COLUMN_NAME_VALUE));

        cursor.close();

        return projection;
    }

    List<Projection> getAll(String type)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(type, projection, null, null, null, null, null);

        List<Projection> projections = new ArrayList<>();

        while (cursor.moveToNext())
        {
            Projection projection = new Projection();
            projection.type = type;
            projection.key = cursor.getString(cursor.getColumnIndexOrThrow(CacheEntry.COLUMN_NAME_KEY));
            projection.data = cursor.getString(cursor.getColumnIndexOrThrow(CacheEntry.COLUMN_NAME_VALUE));
            projections.add(projection);
        }

        cursor.close();

        return projections;
    }

    List<Projection> startsWith(String type, String startKey)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(type, projection, query, new String[] { startKey + '%' }, null, null, null);

        List<Projection> projections = new ArrayList<>();

        while (cursor.moveToNext())
        {
            Projection projection = new Projection();
            projection.type = type;
            projection.key = cursor.getString(cursor.getColumnIndexOrThrow(CacheEntry.COLUMN_NAME_KEY));
            projection.data = cursor.getString(cursor.getColumnIndexOrThrow(CacheEntry.COLUMN_NAME_VALUE));
            projections.add(projection);
        }

        cursor.close();

        return projections;
    }

    long count(String type, String startKey)
    {
        SQLiteDatabase db = getReadableDatabase();
        if (startKey == null || startKey.trim().isEmpty())
            return DatabaseUtils.queryNumEntries(db, type);
        return DatabaseUtils.queryNumEntries(db, type, CacheEntry.COLUMN_NAME_KEY + " like ?", new String[] { startKey + '%' });
    }
}
