package com.mental_elemental.android.support.db;

import com.mental_elemental.android.support.Serializer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.FutureTask;

import androidx.annotation.NonNull;

public class SimpleSavableCollection<T>
{
    private final Savables<T> savables;

    public SimpleSavableCollection(@NonNull DbHelper dbHelper, @NonNull String type, @NonNull Serializer<T> serializer)
    {
        savables = new Savables<T>(dbHelper, type, serializer)
        {
            @Override
            protected void setId(String key, T obj)
            {
                SimpleSavableCollection.this.setKey(key, obj);
            }
        };
    }

    public List<T> getAll()
    {
        return getAll(null);
    }

    public List<T> getAll(String startKey)
    {
        try
        {
            List<T> all = savables.getAll(startKey).get();
            for (T obj : all)
                setTemporary(obj, false);
            return all;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return new ArrayList<T>();
    }

    public FutureTask<String> save(T obj)
    {
        String key = getKey(obj);
        setTemporary(obj, false);
        return savables.save(key, obj);
    }

    public FutureTask<String> remove(T obj)
    {
        setTemporary(obj, true);
        return savables.remove(getKey(obj));
    }

    public FutureTask<List<String>> remove(List<T> objs)
    {
        List<String> keys = new ArrayList<>();
        for (T obj : objs)
        {
            setTemporary(obj, true);
            keys.add(getKey(obj));
        }

        return savables.remove(keys);
    }

    public long count()
    {
        try
        {
            return savables.count().get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    public long count(String startKey)
    {
        try
        {
            return savables.count(startKey).get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    public T get(String key)
    {
        try
        {
            T obj = savables.get(key).get();
            if (obj != null)
                setTemporary(obj, false);
            return obj;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public boolean exists(T o)
    {
        try
        {
            return savables.count(getKey(o)).get() > 0;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    protected void setKey(String key, T obj)
    {

    }

    protected void setTemporary(T obj, boolean temporary)
    {

    }

    public FutureTask<Void> clear()
    {
        return savables.clear();
    }

    public FutureTask<String> remove(String key)
    {
        return savables.remove(key);
    }

    protected String getKey(T o)
    {
        return UUID.randomUUID().toString();
    }
}
