package com.mental_elemental.android.support.db;

import com.mental_elemental.android.support.Serializer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;

import androidx.annotation.NonNull;

public abstract class SingleInstanceSavableCollection<T> extends SimpleSavableCollection<T>
{
    protected Map<String, T> cacheByKey = new HashMap<>(100);
    private boolean requestedAll = false;

    public SingleInstanceSavableCollection(@NonNull DbHelper dbHelper, @NonNull String type, @NonNull Serializer<T> serializer)
    {
        super(dbHelper, type, serializer);
    }

    @Override
    public List<T> getAll()
    {
        if (!requestedAll)
        {
            Collection<T> all = super.getAll();
            for (T o : all)
            {
                String key = getKey(o);
                if (!cacheByKey.containsKey(key))
                    cacheByKey.put(key, o);
            }

            requestedAll = true;
        }

        return new ArrayList<>(cacheByKey.values());
    }

    @Override
    public List<T> getAll(String startKey)
    {
        List<T> all = super.getAll(startKey);
        // Check data integrity
        for (T o : all)
        {
            String key = getKey(o);
            if (!cacheByKey.containsKey(key))
                cacheByKey.put(key, o);
        }

        return new ArrayList<>(cacheByKey.values());
    }

    public FutureTask<String> save(T obj)
    {
        String key = getKey(obj);
        cacheByKey.remove(key);
        cacheByKey.put(key, obj);
        return super.save(obj);
    }

    @Override
    public long count(String startKey)
    {
        if (requestedAll)
            return cacheByKey.values().size();
        return super.count(startKey);
    }

    @Override
    public T get(String key)
    {
        T obj = cacheByKey.get(key);
        if (obj == null)
        {
            obj = super.get(key);
            if (obj != null)
                cacheByKey.put(key, obj);
        }
        return obj;
    }

    public boolean exists(T o)
    {
        if (cacheByKey.containsKey(getKey(o)))
            return true;
        return super.exists(o);
    }

    public void destroy(T o)
    {
        remove(o);
        cacheByKey.remove(getKey(o));
    }

}
