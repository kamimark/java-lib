package com.mental_elemental.android.support.db;

import com.mental_elemental.android.support.Serializer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import androidx.annotation.NonNull;

public abstract class Savables<T>
{
    protected DbHelper dbHelper;
    protected final String type;
    protected final Serializer<T> serializer;
    protected Executor executor = Executors.newSingleThreadExecutor();

    protected Savables(@NonNull DbHelper dbHelper, @NonNull String type, @NonNull Serializer<T> serializer)
    {
        this.dbHelper = dbHelper;
        this.type = type;
        this.serializer = serializer;
    }

    protected abstract void setId(String key, T obj);

    public FutureTask<Void> clear()
    {
        FutureTask<Void> future = new FutureTask<>(() -> {
            dbHelper.remove(type, null);
            return null;
        });

        executor.execute(future);
        return future;
    }

    public FutureTask<List<T>> getAll()
    {
        return getAll(null);
    }

    public FutureTask<List<T>> getAll(String startKey)
    {
        FutureTask<List<T>> future = new FutureTask<>(() -> {
            List<T> container = new ArrayList<T>();
            List<DbHelper.Projection> projections = startKey == null ? dbHelper.getAll(type) : dbHelper.startsWith(type, startKey);
            for (DbHelper.Projection projection : projections)
            {
                T obj = serializer.deserialize(projection.data);
                if (obj != null)
                {
                    container.add(obj);
                    setId(projection.key, obj);
                }
            }
            return container;
        });

        executor.execute(future);
        return future;
    }

    public FutureTask<Long> count()
    {
        return count(null);
    }

    public FutureTask<Long> count(String startKey)
    {
        FutureTask<Long> future = new FutureTask<>(() -> dbHelper.count(type, startKey));
        executor.execute(future);
        return future;
    }

    public FutureTask<String> save(String key, T obj)
    {
        FutureTask<String> future = new FutureTask<>(() -> {
            dbHelper.set(type, key, serializer.serialize(obj));
            return key;
        });

        executor.execute(future);
        return future;
    }

    public FutureTask<String> remove(String key)
    {
        FutureTask<String> future = new FutureTask<>(() -> {
            dbHelper.remove(type, key);
            return key;
        });

        executor.execute(future);
        return future;
    }

    public FutureTask<List<String>> remove(List<String> keys)
    {
        FutureTask<List<String>> future = new FutureTask<>(() -> {
            for (String key : keys)
                dbHelper.remove(type, key);
            return keys;
        });

        executor.execute(future);
        return future;
    }

    public FutureTask<T> get(String key)
    {
        FutureTask<T> future = new FutureTask<>(() -> {
            DbHelper.Projection projection = dbHelper.get(type, key);
            return serializer.deserialize(projection.data);
        });

        executor.execute(future);
        return future;
    }
}
