package com.mental_elemental.android.support;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class Support
{
    private static final Map<String, Dialog> dialogs = new HashMap<>();
    private static Executor executor = Executors.newFixedThreadPool(3);

    public static Runnable RUNNABLE_DO_NOTHING = () -> {

    };

    public static Response.ErrorListener NO_ERROR_LISTENER = error -> {

    };

    public static Iterable<View> getChildViews(final ViewGroup viewGroup)
    {
        return () -> new Iterator<View>()
        {
            int i = 0;

            @Override
            public boolean hasNext()
            {
                return i < viewGroup.getChildCount();
            }

            @Override
            public View next()
            {
                View view = viewGroup.getChildAt(i);
                ++i;
                return view;
            }
        };
    }

    public static int brighter(int color, float ratio)
    {
        float[] hsv = new float[3];
        android.graphics.Color.colorToHSV(color, hsv);
        hsv[2] += ratio;
        return android.graphics.Color.HSVToColor(hsv);
    }

    public static AlertDialog createDialog(AlertDialog.Builder builder, final String title)
    {
        if (dialogs.containsKey(title))
            return null;

        builder.setTitle(title);
        builder.setOnDismissListener(dialog -> dialogs.remove(title));

        AlertDialog dialog = builder.create();
        dialogs.put(title, dialog);

        return dialog;
    }

    public static CharSequence fromDouble(double number)
    {
        long roundedNumber = Math.round(number);
        if (number == roundedNumber)
            return String.valueOf(roundedNumber);

        return String.valueOf(1f * Math.round(number * 100) / 100);
    }

    public static String ordinal(int i)
    {
        String[] sufixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        switch (i % 100)
        {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];
        }
    }

    public static void displayError(Context context, String title, String error)
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(title)
                .setMessage(error)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    public static void report(@NonNull Context context, Exception exception)
    {
        exception.printStackTrace();
//        if ((context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) == 0)
//            try
//            {
//                Crashlytics.logException(exception);
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
    }

    public static boolean equals(String str1, String str2)
    {
        return str1 == null ? str2 == null : str1.equals(str2);
    }

    public static <T> boolean hasEqual(Collection<T> collection, T obj)
    {
        for (T o : collection)
            if (o.equals(obj))
                return true;
        return false;
    }

    public static <T> void chain(final Queue<T> arguments, final Chainable<T> chainable)
    {
        if (arguments.isEmpty())
            return;

        T argument = arguments.poll();
        chainable.run(argument, () -> chain(arguments, chainable), arguments.isEmpty());
    }

    public static boolean equals(SparseArray<Double> sparseArray, SparseArray<Double> otherSparceArray)
    {
        if (sparseArray.size() != otherSparceArray.size())
            return false;

        for (int i = 0; i < sparseArray.size(); ++i)
        {
            if (sparseArray.keyAt(i) != otherSparceArray.keyAt(i))
                return false;
            int key = sparseArray.keyAt(i);
            if (!sparseArray.get(key).equals(otherSparceArray.get(key)))
                return false;
        }

        return true;
    }

    public static int[] getInts(JSONArray array) throws JSONException
    {
        int[] values = new int[array.length()];
        for (int i = 0; i < array.length(); ++i)
            values[i] = array.getInt(i);
        return values;
    }

    public static JSONArray jsonize(double[] doubles) throws JSONException
    {
        JSONArray jsonArray = new JSONArray();
        for (double d : doubles)
            jsonArray.put(d == Double.MIN_VALUE ? "X" : d);
        return jsonArray;
    }

    public static String toString(double value)
    {
        long intValue = Math.round(value);
        if (value == intValue)
            return String.valueOf(intValue);

        return String.valueOf(Math.round(value * 100) / 100);
    }

    public static int compare(long timestamp, long timestamp1)
    {
        long diff = timestamp - timestamp1;
        if (diff < 0)
            return -1;
        else if (diff > 0)
            return 1;
        return 0;
    }

    public static <T> boolean remove(Collection<T> collection, T object)
    {
        for (T obj : collection)
        {
            if (obj.equals(object))
            {
                collection.remove(obj);
                return true;
            }
        }

        return false;
    }

    public static <T> boolean contains(Collection<T> collection, T object)
    {
        for (T obj : collection)
            if (obj.equals(object))
                return true;
        return false;
    }

    public static Long parseLong(String longStr)
    {
        if (longStr == null || longStr.isEmpty())
            return null;

        try
        {
            return Long.parseLong(longStr);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static String parseNetworkStringResponse(NetworkResponse response)
    {
        try
        {
            return new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        }
        catch (UnsupportedEncodingException e)
        {
            return new String(response.data);
        }
    }

    public static String nullifyEmpty(String value)
    {
        if (value == null || value.trim().isEmpty())
            return null;
        return value;
    }

    public static void notifyChanges(RecyclerView.Adapter adapter, List original, List selectedPlayers)
    {
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback()
        {
            @Override
            public int getOldListSize()
            {
                return original.size();
            }

            @Override
            public int getNewListSize()
            {
                return selectedPlayers.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
            {
                return original.get(oldItemPosition) == selectedPlayers.get(newItemPosition);
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
            {
                return original.get(oldItemPosition) == selectedPlayers.get(newItemPosition);
            }
        }, true);

        result.dispatchUpdatesTo(adapter);
    }

    public static void enableAutoCloseSoftKeyboard(EditText edit)
    {
        edit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edit.setOnEditorActionListener((TextView textView, int actionId, KeyEvent keyEvent) ->
        {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) textView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                assert(imm != null);
                imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                return true;
            }
            return false;
        });

    }

    public static void dismissKeyboard(View view)
    {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert(imm != null);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void forceBottomSheetHide(BottomSheetBehavior sheetBehavior)
    {
        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState)
            {
                if (newState == BottomSheetBehavior.STATE_HALF_EXPANDED || newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    sheetBehavior.removeBottomSheetCallback(this);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset)
            {
            }
        });
    }

    public static void setImageBitmap(ImageView imageView, View progressView, String iconUrl)
    {
        Downloader.download(imageView.getContext(), iconUrl, (url, fileLocation) -> {
            if (ViewCompat.isAttachedToWindow(imageView))
            {
                progressView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                Support.setImageBitmap(imageView, fileLocation);
            }
        });
    }

    public interface Chainable<T>
    {
        void run(T arg, Runnable completed, boolean lastOne);
    }

    public static void setImageBitmap(ImageView imageView, String localImagePath)
    {
        String fullFilePath = imageView.getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + localImagePath;
        File imageFile = new File(fullFilePath);
        if (!imageFile.exists())
            return;

        imageView.post(() -> {
            executor.execute(() -> {
                final File thumbnail = getThumbnailFile(imageFile, imageView);
                new Handler(Looper.getMainLooper()).post(() ->{
                    if (ViewCompat.isAttachedToWindow(imageView))
                        imageView.setImageURI(Uri.fromFile(thumbnail));
                });
            });
        });
    }

    private static File getThumbnailFile(File imageFile, ImageView imageView)
    {
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int photoArea = photoH * photoW;
        int imageViewArea = targetH * targetW;

        if (photoArea < (512 * 512) || imageViewArea >= photoArea || 1f * photoArea / imageViewArea < 1.5f)
            return imageFile;

        String thumbnailFolderPath = imageFile.getParent() + File.separator + "thumbs";
        File thumbnailFolder = new File(thumbnailFolderPath);

        if (!thumbnailFolder.exists())
            if (!thumbnailFolder.mkdirs())
                new RuntimeException("Cannot create folder for thumbs").printStackTrace();

        int thumbSize = 1024;
        while (targetH > thumbSize || targetW > thumbSize)
            thumbSize += 1024;

        File thumbnail = new File(thumbnailFolderPath + File.separator + imageFile.getName() + '_' + thumbSize);
        if (thumbnail.exists())
            return thumbnail;

        System.out.println("Creating image thumbnail: " + thumbnail.getName());
        int scaleFactor = Math.max(photoW, photoH) / thumbSize;
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);

        try
        {
            if (!thumbnail.createNewFile())
                throw new RuntimeException("Cannot create thumbnail for image");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 , bos);
            byte[] bitMapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(thumbnail);
            fos.write(bitMapData);
            fos.flush();
            fos.close();
            return thumbnail;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return imageFile;
        }
    }

    public static void openInputDialog(Context context, String title, String actionLabel, String defaultValue, final SingleArgumentFunction<Boolean, String> callable)
    {
        openInputDialog(context, title, actionLabel, defaultValue, callable, null, null);
    }

    public static void openInputDialog(Context context, String title, String actionLabel, String defaultValue, final SingleArgumentFunction<Boolean, String> callable, String neutralActionLabel,final SingleArgumentFunction<Boolean, String> neutral)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final EditText editText = new EditText(context);
        editText.setImeActionLabel(actionLabel, KeyEvent.KEYCODE_ENTER);
        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editText.setSingleLine();
        editText.setText(defaultValue);

        builder.setView(editText);
        builder.setPositiveButton(actionLabel, null);
        if (neutralActionLabel != null)
            builder.setNeutralButton(neutralActionLabel, null);

        final AlertDialog dialog = Support.createDialog(builder, title);
        if (dialog == null)
            return;

        dialog.setOnShowListener(dialogInterface -> {
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(button -> {
                if (callable.call(editText.getText().toString()))
                    dialog.dismiss();
            });
            if (neutral != null)
                dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(button -> {
                    if (neutral.call(editText.getText().toString()))
                        dialog.dismiss();
                });
        });

        dialog.show();

        editText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId != KeyEvent.KEYCODE_ENTER
                    && keyEvent.getKeyCode() != KeyEvent.KEYCODE_ENTER)
                return false;

            boolean result = callable.call(editText.getText().toString());

            if (result)
                dialog.dismiss();

            return result;
        });
    }

    public static int indexOf(int[] array, int element)
    {
        for (int i = 0; i < array.length; ++i)
            if (array[i] == element)
                return i;
        return -1;
    }

    public static int indexOf(Object[] standardDice, Object die)
    {
        for (int i = 0; i < standardDice.length; ++i)
            if (standardDice[i] == standardDice)
                return i;
        return -1;
    }

    public static void setDrawable(ImageView view, int res)
    {
        view.setImageDrawable(ContextCompat.getDrawable(view.getContext(), res));
    }

    public static void setColorFilter(ImageView view, int res)
    {
        view.setColorFilter(ContextCompat.getColor(view.getContext(), res));
    }

    public static void setTextColor(TextView view, int res)
    {
        view.setTextColor(ContextCompat.getColor(view.getContext(), res));
    }

    public static void setBackgroundColor(View view, int res)
    {
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), res));
    }

    public static boolean isEmptyOrNull(String string)
    {
        return string == null || string.trim().isEmpty();
    }

    public static <T> T get(T string, T fallback)
    {
        return string == null ? fallback : string;
    }

    public abstract static class TextWatcher implements android.text.TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
        {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
        {

        }
    }

    public static class Color {
        public static int arbg(int a, int r, int g, int b) {
            return (a & 0xFF) << 24 | (r & 0xff) << 16 | (g & 0xff) << 8 | (b & 0xff);
        }

        public static int arbg(float a, float r, float g, float b) {
            int ai = (int) (a * 0xFF);
            int ri = (int) (r * 0xFF);
            int gi = (int) (g * 0xFF);
            int bi = (int) (b * 0xFF);
            return arbg(ai, ri, gi, bi);
        }

        public static int rbg(int r, int g, int b) {
            return arbg(0xFF, r, g, b);
        }

        public static int rbg(float r, float g, float b) {
            return arbg(1f, r, g, b);
        }

        public static int grey(float x) {
            return arbg(1f, x, x, x);
        }

        public static int perc(int color, float perc)
        {
            int a = (color >> 24) & 0xFF;
            int r = (color >> 16) & 0xFF;
            int g = (color >> 8) & 0xFF;
            int b = (color) & 0xFF;
            return arbg(a, (int)(r * perc), (int)(g * perc), (int)(b * perc));
        }
    }

    public static byte[] compress(String data) throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
        GZIPOutputStream gzip = new GZIPOutputStream(bos);
        gzip.write(data.getBytes());
        gzip.close();
        byte[] compressed = bos.toByteArray();
        bos.close();
        return compressed;
    }

    public static String decompress(byte[] data) throws IOException
    {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        GZIPInputStream gunzip = new GZIPInputStream(bis);
        int chunkSize = 1024 * 1024;
        byte[] readBuffer = new byte[chunkSize];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while (true)
        {
            int bytesRead = gunzip.read(readBuffer, 0, readBuffer.length);
            bos.write(readBuffer, 0, bytesRead);
            if (bytesRead < chunkSize)
                break;
        }
        gunzip.close();
        bis.close();
        byte[] output = bos.toByteArray();
        bos.close();
        return new String(output);
    }
}